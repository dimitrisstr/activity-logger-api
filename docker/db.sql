-- CREATE TABLE Account (
--    "account_id" SERIAL NOT NULL,
--    "first_name" VARCHAR(32) NOT NULL,
--    "last_name" VARCHAR(32) NOT NULL,
--    "username" VARCHAR(32) NOT NULL,
--    "email" VARCHAR(32) NOT NULL,
--    "created_at" TIMESTAMP NOT NULL,
--    PRIMARY KEY ("account_id")
-- );

CREATE TABLE Tag (
	"tag_id" SERIAL NOT NULL,
	"name" VARCHAR(32) NOT NULL,
	"created_at" TIMESTAMP NOT NULL DEFAULT NOW(),
	PRIMARY KEY ("tag_id")
);

CREATE TABLE Activity (
	"activity_id" SERIAL NOT NULL,
	"name" VARCHAR(32) NOT NULL,
	"description" VARCHAR(128) NOT NULL,
	"completed" BOOLEAN DEFAULT FALSE,
	"created_at" TIMESTAMP NOT NULL DEFAULT NOW(),
	PRIMARY KEY ("activity_id")
);

CREATE TABLE ActivityTag (
    "activity_id" SERIAL NOT NULL,
    "tag_id" SERIAL NOT NULL,
    PRIMARY KEY ("activity_id", "tag_id"),
    FOREIGN KEY ("activity_id") REFERENCES Activity ("activity_id"),
    FOREIGN KEY ("tag_id") REFERENCES Tag ("tag_id")
);

CREATE TABLE Session (
	"session_id" SERIAL NOT NULL,
	"activity_id" INT NOT NULL,
	"session_date" DATE NOT NULL,
	"duration" TIME NOT NULL,
	PRIMARY KEY ("session_id"),
	FOREIGN KEY ("activity_id") REFERENCES Activity ("activity_id")
);

