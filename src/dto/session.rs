use crate::model::session::{CreateSession, Session, UpdateSession};
use chrono::{NaiveDate, NaiveTime};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct CreateSessionRequest {
    pub day: NaiveDate,
    pub duration: NaiveTime,
}

#[derive(Deserialize)]
pub struct UpdateSessionRequest {
    pub day: Option<NaiveDate>,
    pub duration: Option<NaiveTime>,
}

#[derive(Serialize)]
pub struct SessionResponse {
    pub session_id: i32,
    pub activity_id: i32,
    pub day: NaiveDate,
    pub duration: NaiveTime,
}

#[derive(Serialize)]
pub struct SessionByActivity {
    pub session_id: i32,
    pub day: NaiveDate,
    pub duration: NaiveTime,
}

#[derive(Serialize)]
pub struct GetSessionsByActivityResponse {
    pub results: Vec<SessionByActivity>,
}

#[derive(Serialize)]
pub struct GetSessionsResponse {
    pub results: Vec<SessionResponse>,
}

impl From<CreateSessionRequest> for CreateSession {
    fn from(request: CreateSessionRequest) -> Self {
        Self {
            duration: request.duration,
            session_date: request.day,
        }
    }
}

impl From<UpdateSessionRequest> for UpdateSession {
    fn from(request: UpdateSessionRequest) -> Self {
        Self {
            duration: request.duration,
            session_date: request.day,
        }
    }
}

impl From<Session> for SessionResponse {
    fn from(session: Session) -> Self {
        Self {
            activity_id: session.activity_id,
            session_id: session.session_id,
            duration: session.duration,
            day: session.session_date,
        }
    }
}

impl From<Session> for SessionByActivity {
    fn from(session: Session) -> Self {
        Self {
            session_id: session.session_id,
            day: session.session_date,
            duration: session.duration,
        }
    }
}
