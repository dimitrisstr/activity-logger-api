use crate::model::tag::{CreateTag, Tag, UpdateTag};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct CreateTagRequest {
    pub name: String,
}

#[derive(Deserialize)]
pub struct UpdateTagRequest {
    pub name: Option<String>,
}

#[derive(Serialize)]
pub struct TagResponse {
    pub tag_id: i32,
    pub name: String,
    pub created_at: NaiveDateTime,
}

#[derive(Serialize)]
pub struct GetTagResponse {
    pub results: Vec<TagResponse>,
}

impl From<CreateTagRequest> for CreateTag {
    fn from(request: CreateTagRequest) -> Self {
        Self { name: request.name }
    }
}

impl From<UpdateTagRequest> for UpdateTag {
    fn from(request: UpdateTagRequest) -> Self {
        Self { name: request.name }
    }
}

impl From<Tag> for TagResponse {
    fn from(tag: Tag) -> Self {
        Self {
            tag_id: tag.tag_id,
            name: tag.name,
            created_at: tag.created_at,
        }
    }
}
