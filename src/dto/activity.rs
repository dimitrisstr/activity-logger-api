use crate::model::activity::{Activity, CreateActivity, TagActivity, UpdateActivity};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct CreateActivityRequest {
    pub name: String,
    pub description: String,
    pub completed: bool,
}

#[derive(Deserialize)]
pub struct UpdateActivityRequest {
    pub name: Option<String>,
    pub description: Option<String>,
    pub completed: Option<bool>,
}

#[derive(Deserialize)]
pub struct TagActivityRequest {
    pub tags: Vec<i32>,
}

#[derive(Serialize)]
pub struct ActivityResponse {
    pub activity_id: i32,
    pub name: String,
    pub description: String,
    pub completed: bool,
    pub created_at: NaiveDateTime,
}

#[derive(Serialize)]
pub struct GetActivitiesResponse {
    pub results: Vec<ActivityResponse>,
}

#[derive(Serialize)]
pub struct GetTagsResponse {
    pub results: Vec<i32>,
}

#[derive(Serialize)]
pub struct TagActivityResponse {
    pub tags: Vec<i32>,
}

impl From<CreateActivityRequest> for CreateActivity {
    fn from(request: CreateActivityRequest) -> Self {
        Self {
            name: request.name,
            description: request.description,
            completed: request.completed,
        }
    }
}

impl From<UpdateActivityRequest> for UpdateActivity {
    fn from(request: UpdateActivityRequest) -> Self {
        Self {
            name: request.name,
            description: request.description,
            completed: request.completed,
        }
    }
}

impl From<TagActivityRequest> for TagActivity {
    fn from(request: TagActivityRequest) -> Self {
        Self { tags: request.tags }
    }
}

impl From<Activity> for ActivityResponse {
    fn from(activity: Activity) -> Self {
        Self {
            activity_id: activity.activity_id,
            name: activity.name,
            description: activity.description,
            completed: activity.completed,
            created_at: activity.created_at,
        }
    }
}
