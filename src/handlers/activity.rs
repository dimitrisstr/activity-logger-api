use crate::dto::activity::{
    ActivityResponse, CreateActivityRequest, GetActivitiesResponse, GetTagsResponse,
    TagActivityRequest, TagActivityResponse, UpdateActivityRequest,
};
use crate::repository::activity_repository::ActivityRepository;
use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::Json;
use sqlx::PgPool;

pub async fn create_activity(
    State(pool): State<PgPool>,
    Json(payload): Json<CreateActivityRequest>,
) -> Result<Json<ActivityResponse>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    match repository.create(payload.into()).await {
        Ok(activity) => Ok(Json(activity.into())),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn update_activity_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
    Json(payload): Json<UpdateActivityRequest>,
) -> Result<Json<ActivityResponse>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    match repository.update_by_id(id, payload.into()).await {
        Ok(Some(activity)) => Ok(Json(activity.into())),
        Ok(None) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_activity_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<ActivityResponse>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    match repository.get_by_id(id).await {
        Ok(Some(activity)) => Ok(Json(activity.into())),
        Ok(None) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn delete_activity_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<()>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    match repository.delete_by_id(id).await {
        Ok(_) => Ok(Json({})),
        Err(sqlx::Error::RowNotFound) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_activities(
    State(pool): State<PgPool>,
) -> Result<Json<GetActivitiesResponse>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    match repository.get_all().await {
        Ok(activities) => {
            let activities_response = activities.into_iter().map(ActivityResponse::from).collect();
            let response = GetActivitiesResponse {
                results: activities_response,
            };
            Ok(Json(response))
        }
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_tags(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<GetTagsResponse>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    match repository.get_tags(id).await {
        Ok(tags) => {
            let tags_response = GetTagsResponse { results: tags };
            Ok(Json(tags_response))
        }
        Err(sqlx::Error::RowNotFound) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn tag_activity(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
    Json(payload): Json<TagActivityRequest>,
) -> Result<Json<TagActivityResponse>, StatusCode> {
    let repository = ActivityRepository::new(pool);
    let tags = payload.tags.clone();
    match repository.tag_activity(id, payload.into()).await {
        Ok(()) => {
            let response = TagActivityResponse { tags };
            Ok(Json(response))
        }
        Err(sqlx::Error::RowNotFound) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
