use crate::dto::session::{
    CreateSessionRequest, GetSessionsByActivityResponse, GetSessionsResponse, SessionByActivity,
    SessionResponse, UpdateSessionRequest,
};
use crate::repository::session_repository::SessionRepository;
use axum::extract::{Path, Query, State};
use axum::http::StatusCode;
use axum::Json;
use chrono::NaiveDate;
use serde::Deserialize;
use sqlx::PgPool;

#[derive(Deserialize)]
pub struct GetSessionsParams {
    from_date: Option<NaiveDate>,
    to_date: Option<NaiveDate>,
}

pub async fn create_session(
    State(pool): State<PgPool>,
    Path(activity_id): Path<i32>,
    Json(payload): Json<CreateSessionRequest>,
) -> Result<Json<SessionResponse>, StatusCode> {
    let repository = SessionRepository::new(pool);
    match repository.create(activity_id, payload.into()).await {
        Ok(session) => Ok(Json(session.into())),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn update_session_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
    Json(payload): Json<UpdateSessionRequest>,
) -> Result<Json<SessionResponse>, StatusCode> {
    let repository = SessionRepository::new(pool);
    match repository.update_by_id(id, payload.into()).await {
        Ok(Some(session)) => Ok(Json(session.into())),
        Ok(None) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_session_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<SessionResponse>, StatusCode> {
    let repository = SessionRepository::new(pool);
    match repository.get_by_id(id).await {
        Ok(Some(session)) => Ok(Json(session.into())),
        Ok(None) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn delete_session_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<()>, StatusCode> {
    let repository = SessionRepository::new(pool);
    match repository.delete_by_id(id).await {
        Ok(_) => Ok(Json({})),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_sessions(
    Query(params): Query<GetSessionsParams>,
    State(pool): State<PgPool>,
) -> Result<Json<GetSessionsResponse>, StatusCode> {
    let repository = SessionRepository::new(pool);
    let from_date = params
        .from_date
        .unwrap_or(NaiveDate::from_ymd_opt(0, 1, 1).unwrap());
    let to_date = params.to_date.unwrap_or(NaiveDate::MAX);

    match repository.get_all(from_date, to_date).await {
        Ok(sessions) => {
            let sessions_response = sessions.into_iter().map(SessionResponse::from).collect();
            let response = GetSessionsResponse {
                results: sessions_response,
            };
            Ok(Json(response))
        }
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_sessions_by_activity(
    State(pool): State<PgPool>,
    Path(activity_id): Path<i32>,
) -> Result<Json<GetSessionsByActivityResponse>, StatusCode> {
    let repository = SessionRepository::new(pool);
    match repository.get_all_by_activity(activity_id).await {
        Ok(sessions) => {
            let sessions_response = sessions.into_iter().map(SessionByActivity::from).collect();
            let response = GetSessionsByActivityResponse {
                results: sessions_response,
            };
            Ok(Json(response))
        }
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
