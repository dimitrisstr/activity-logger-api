use crate::dto::tag::{CreateTagRequest, GetTagResponse, TagResponse, UpdateTagRequest};
use crate::repository::tag_repository::TagRepository;
use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::Json;
use sqlx::PgPool;

pub async fn create_tag(
    State(pool): State<PgPool>,
    Json(payload): Json<CreateTagRequest>,
) -> Result<Json<TagResponse>, StatusCode> {
    let repository = TagRepository::new(pool);
    match repository.create(payload.into()).await {
        Ok(tag) => Ok(Json(tag.into())),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn update_tag_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
    Json(payload): Json<UpdateTagRequest>,
) -> Result<Json<TagResponse>, StatusCode> {
    let repository = TagRepository::new(pool);
    match repository.update_by_id(id, payload.into()).await {
        Ok(Some(tag)) => Ok(Json(tag.into())),
        Ok(None) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_tag_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<TagResponse>, StatusCode> {
    let repository = TagRepository::new(pool);
    match repository.get_by_id(id).await {
        Ok(Some(tag)) => Ok(Json(tag.into())),
        Ok(None) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn delete_tag_by_id(
    State(pool): State<PgPool>,
    Path(id): Path<i32>,
) -> Result<Json<()>, StatusCode> {
    let repository = TagRepository::new(pool);
    match repository.delete_by_id(id).await {
        Ok(_) => Ok(Json({})),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn get_tags(State(pool): State<PgPool>) -> Result<Json<GetTagResponse>, StatusCode> {
    let repository = TagRepository::new(pool);
    match repository.get_all().await {
        Ok(tags) => {
            let tags_response = tags.into_iter().map(TagResponse::from).collect();
            let response = GetTagResponse {
                results: tags_response,
            };
            Ok(Json(response))
        }
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
