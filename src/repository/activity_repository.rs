use crate::model::activity::{Activity, CreateActivity, TagActivity, UpdateActivity};
use sqlx::PgPool;

#[derive(Clone)]
pub struct ActivityRepository {
    pool: PgPool,
}

impl ActivityRepository {
    pub fn new(pool: PgPool) -> Self {
        Self { pool }
    }

    pub async fn create(&self, create_activity: CreateActivity) -> Result<Activity, sqlx::Error> {
        let activity = sqlx::query_as(
            "INSERT INTO Activity (name, description, completed) VALUES ($1, $2, $3) RETURNING *",
        )
        .bind(create_activity.name)
        .bind(create_activity.description)
        .bind(create_activity.completed)
        .fetch_one(&self.pool)
        .await?;

        Ok(activity)
    }

    pub async fn update_by_id(
        &self,
        activity_id: i32,
        update_activity: UpdateActivity,
    ) -> Result<Option<Activity>, sqlx::Error> {
        let activity = sqlx::query_as(
            "UPDATE Activity \
                 SET \
                    name = COALESCE($1, name), \
                    description = COALESCE($2, description), \
                    completed = COALESCE($3, completed) \
                 WHERE activity_id = $4 RETURNING *",
        )
        .bind(update_activity.name)
        .bind(update_activity.description)
        .bind(update_activity.completed)
        .bind(activity_id)
        .fetch_optional(&self.pool)
        .await?;

        Ok(activity)
    }

    pub async fn delete_by_id(&self, activity_id: i32) -> Result<(), sqlx::Error> {
        if self.get_by_id(activity_id).await?.is_none() {
            return Err(sqlx::Error::RowNotFound);
        }

        sqlx::query("DELETE FROM Activity WHERE activity_id = $1")
            .bind(activity_id)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    pub async fn get_by_id(&self, activity_id: i32) -> Result<Option<Activity>, sqlx::Error> {
        let activity = sqlx::query_as("SELECT * FROM Activity WHERE activity_id = $1")
            .bind(activity_id)
            .fetch_optional(&self.pool)
            .await?;

        Ok(activity)
    }

    pub async fn get_all(&self) -> Result<Vec<Activity>, sqlx::Error> {
        let activities = sqlx::query_as("SELECT * FROM Activity")
            .fetch_all(&self.pool)
            .await?;

        Ok(activities)
    }

    pub async fn get_tags(&self, activity_id: i32) -> Result<Vec<i32>, sqlx::Error> {
        if self.get_by_id(activity_id).await?.is_none() {
            return Err(sqlx::Error::RowNotFound);
        }

        let tag_ids = sqlx::query_scalar(
            "SELECT tag_id FROM ActivityTag JOIN Tag USING (tag_id) WHERE activity_id = $1",
        )
        .bind(activity_id)
        .fetch_all(&self.pool)
        .await?;

        Ok(tag_ids)
    }

    pub async fn tag_activity(
        &self,
        activity_id: i32,
        tag_activity: TagActivity,
    ) -> Result<(), sqlx::Error> {
        if self.get_by_id(activity_id).await?.is_none() {
            return Err(sqlx::Error::RowNotFound);
        }

        let tag_ids_exist: bool =
            sqlx::query_scalar("SELECT COUNT(*) = $1 FROM Tag WHERE tag_id = ANY($2)")
                .bind(tag_activity.tags.len() as i32)
                .bind(&tag_activity.tags)
                .fetch_one(&self.pool)
                .await?;

        if !tag_ids_exist {
            return Err(sqlx::Error::RowNotFound);
        }

        sqlx::query("DELETE FROM ActivityTag WHERE activity_id = $1")
            .bind(activity_id)
            .execute(&self.pool)
            .await?;

        sqlx::query(
            "INSERT INTO ActivityTag (activity_id, tag_id)\
                 SELECT $1, UNNEST($2::int[])",
        )
        .bind(activity_id)
        .bind(tag_activity.tags)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}
