use crate::model::tag::{CreateTag, Tag, UpdateTag};
use sqlx::PgPool;

#[derive(Clone)]
pub struct TagRepository {
    pool: PgPool,
}

impl TagRepository {
    pub fn new(pool: PgPool) -> Self {
        Self { pool }
    }

    pub async fn create(&self, create_tag: CreateTag) -> Result<Tag, sqlx::Error> {
        let tag = sqlx::query_as("INSERT INTO Tag (name) VALUES ($1) RETURNING *")
            .bind(create_tag.name)
            .fetch_one(&self.pool)
            .await?;

        Ok(tag)
    }

    pub async fn update_by_id(
        &self,
        tag_id: i32,
        update_tag: UpdateTag,
    ) -> Result<Option<Tag>, sqlx::Error> {
        let tag = sqlx::query_as(
            "UPDATE Tag \
                 SET \
                    name = COALESCE($1, name) \
                 WHERE tag_id = $2 RETURNING *",
        )
        .bind(update_tag.name)
        .bind(tag_id)
        .fetch_optional(&self.pool)
        .await?;

        Ok(tag)
    }

    pub async fn delete_by_id(&self, tag_id: i32) -> Result<(), sqlx::Error> {
        sqlx::query("DELETE FROM Tag WHERE tag_id = $1")
            .bind(tag_id)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    pub async fn get_by_id(&self, tag_id: i32) -> Result<Option<Tag>, sqlx::Error> {
        let tag = sqlx::query_as("SELECT * FROM Tag WHERE tag_id = $1")
            .bind(tag_id)
            .fetch_optional(&self.pool)
            .await?;

        Ok(tag)
    }

    pub async fn get_all(&self) -> Result<Vec<Tag>, sqlx::Error> {
        let tags = sqlx::query_as("SELECT * FROM Tag")
            .fetch_all(&self.pool)
            .await?;

        Ok(tags)
    }
}
