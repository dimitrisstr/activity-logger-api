use crate::model::session::{CreateSession, Session, UpdateSession};
use chrono::NaiveDate;
use sqlx::PgPool;

#[derive(Clone)]
pub struct SessionRepository {
    pool: PgPool,
}

impl SessionRepository {
    pub fn new(pool: PgPool) -> Self {
        Self { pool }
    }

    pub async fn create(
        &self,
        activity_id: i32,
        create_session: CreateSession,
    ) -> Result<Session, sqlx::Error> {
        let session = sqlx::query_as(
            "INSERT INTO Session (activity_id, session_date, duration) VALUES ($1, $2, $3) RETURNING *",
        )
        .bind(activity_id)
        .bind(create_session.session_date)
        .bind(create_session.duration)
        .fetch_one(&self.pool)
        .await?;

        Ok(session)
    }

    pub async fn update_by_id(
        &self,
        session_id: i32,
        update_session: UpdateSession,
    ) -> Result<Option<Session>, sqlx::Error> {
        let session = sqlx::query_as(
            "UPDATE Session \
                 SET \
                    session_date = COALESCE($1, session_date), \
                    duration = COALESCE($2, duration) \
                 WHERE session_id = $3 RETURNING *",
        )
        .bind(update_session.session_date)
        .bind(update_session.duration)
        .bind(session_id)
        .fetch_optional(&self.pool)
        .await?;

        Ok(session)
    }

    pub async fn delete_by_id(&self, session_id: i32) -> Result<(), sqlx::Error> {
        sqlx::query("DELETE FROM Session WHERE session_id = $1")
            .bind(session_id)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    pub async fn get_by_id(&self, session_id: i32) -> Result<Option<Session>, sqlx::Error> {
        let session = sqlx::query_as("SELECT * FROM Session WHERE session_id = $1")
            .bind(session_id)
            .fetch_optional(&self.pool)
            .await?;

        Ok(session)
    }

    pub async fn get_all(
        &self,
        from_date: NaiveDate,
        to_date: NaiveDate,
    ) -> Result<Vec<Session>, sqlx::Error> {
        let sessions = sqlx::query_as("SELECT * FROM Session WHERE session_date BETWEEN $1 AND $2")
            .bind(from_date)
            .bind(to_date)
            .fetch_all(&self.pool)
            .await?;

        Ok(sessions)
    }

    pub async fn get_all_by_activity(&self, activity_id: i32) -> Result<Vec<Session>, sqlx::Error> {
        let sessions = sqlx::query_as("SELECT * FROM Session WHERE activity_id = $1")
            .bind(activity_id)
            .fetch_all(&self.pool)
            .await?;

        Ok(sessions)
    }
}
