use crate::handlers::activity;
use crate::handlers::activity::{
    create_activity, delete_activity_by_id, get_activities, get_activity_by_id, tag_activity,
    update_activity_by_id,
};
use crate::handlers::session::{
    create_session, delete_session_by_id, get_session_by_id, get_sessions,
    get_sessions_by_activity, update_session_by_id,
};
use crate::handlers::tag::{
    create_tag, delete_tag_by_id, get_tag_by_id, get_tags, update_tag_by_id,
};
use axum::routing::get;
use axum::Router;
use sqlx::{Pool, Postgres};
use tower_http::cors::CorsLayer;

pub fn api_router(pool: Pool<Postgres>) -> Router {
    Router::new()
        .nest(
            "/api",
            Router::new()
                .nest("/activities", activities_router())
                .nest("/tags", tags_router())
                .nest("/sessions", sessions_router()),
        )
        .with_state(pool)
        .layer(CorsLayer::permissive())
}

fn activities_router() -> Router<Pool<Postgres>> {
    Router::new()
        .route("/", get(get_activities).post(create_activity))
        .route(
            "/:activity_id",
            get(get_activity_by_id)
                .put(update_activity_by_id)
                .delete(delete_activity_by_id),
        )
        .route(
            "/:activity_id/sessions",
            get(get_sessions_by_activity).post(create_session),
        )
        .route(
            "/:activity_id/tags",
            get(activity::get_tags).put(tag_activity),
        )
}

fn tags_router() -> Router<Pool<Postgres>> {
    Router::new()
        .route("/", get(get_tags).post(create_tag))
        .route(
            "/:tag_id",
            get(get_tag_by_id)
                .put(update_tag_by_id)
                .delete(delete_tag_by_id),
        )
}

fn sessions_router() -> Router<Pool<Postgres>> {
    Router::new().route("/", get(get_sessions)).route(
        "/:session_id",
        get(get_session_by_id)
            .put(update_session_by_id)
            .delete(delete_session_by_id),
    )
}
