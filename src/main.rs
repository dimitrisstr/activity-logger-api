use crate::config::{DbConfig, ServerConfig};
use ::config::{Config, File};
use sqlx::postgres::PgPoolOptions;
use tracing::Level;

mod config;
mod dto;
mod handlers;
mod model;
mod repository;
mod routes;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::TRACE)
        .init();
    std::env::set_var("SQLX_LOG", "debug");

    let configuration = Config::builder()
        .add_source(File::with_name("config.yml"))
        .build()
        .expect("Configuration error.");

    let db_config = configuration
        .get::<DbConfig>("db")
        .expect("Invalid database configuration.");
    let server_config = configuration
        .get::<ServerConfig>("server")
        .expect("Invalid server configuration");

    let pool = PgPoolOptions::new()
        .max_connections(100)
        .connect(db_config.get_url().as_str())
        .await
        .expect("Unable to connect to the database.");

    axum::Server::bind(&server_config.get_url().parse().unwrap())
        .serve(routes::api_router(pool).into_make_service())
        .await
        .unwrap();
}
