use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct DbConfig {
    user: String,
    password: String,
    url: String,
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct ServerConfig {
    port: String,
}

impl DbConfig {
    pub fn get_url(&self) -> String {
        format!(
            "postgres://{}:{}@{}/{}",
            self.user, self.password, self.url, self.name
        )
    }
}

impl ServerConfig {
    pub fn get_url(&self) -> String {
        format!("0.0.0.0:{}", self.port)
    }
}
