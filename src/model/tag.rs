use chrono::NaiveDateTime;
use sqlx::FromRow;

#[derive(FromRow)]
pub struct Tag {
    pub tag_id: i32,
    pub name: String,
    pub created_at: NaiveDateTime,
}

pub struct CreateTag {
    pub name: String,
}

pub struct UpdateTag {
    pub name: Option<String>,
}
