use chrono::NaiveDateTime;
use sqlx::FromRow;

#[derive(FromRow)]
pub struct Activity {
    pub activity_id: i32,
    pub name: String,
    pub description: String,
    pub completed: bool,
    pub created_at: NaiveDateTime,
}

pub struct CreateActivity {
    pub name: String,
    pub description: String,
    pub completed: bool,
}

pub struct UpdateActivity {
    pub name: Option<String>,
    pub description: Option<String>,
    pub completed: Option<bool>,
}

pub struct TagActivity {
    pub tags: Vec<i32>,
}
