use chrono::{NaiveDate, NaiveTime};
use sqlx::FromRow;

#[derive(FromRow)]
pub struct Session {
    pub session_id: i32,
    pub activity_id: i32,
    pub session_date: NaiveDate,
    pub duration: NaiveTime,
}

pub struct CreateSession {
    pub session_date: NaiveDate,
    pub duration: NaiveTime,
}

pub struct UpdateSession {
    pub session_date: Option<NaiveDate>,
    pub duration: Option<NaiveTime>,
}
